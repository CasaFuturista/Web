function changeModal(title, imageSrc, imageAlt, description) {
  const modalTitle = document.getElementById('modal-title');
  const modalImage = document.getElementById('modal-image');
  const modalDescription = document.getElementById('modal-description');

  modalTitle.textContent = title;
  modalImage.setAttribute('src', imageSrc);
  modalImage.setAttribute('alt', imageAlt);
  modalDescription.textContent = description;
}

Array.from(document.getElementsByClassName('futuristic-object')).forEach((futuristicObject) => {
  futuristicObject.addEventListener('click', () => {
    const title = futuristicObject.getAttribute('data-title');
    const imageSrc = futuristicObject.getAttribute('data-imagesrc');
    const imageAlt = futuristicObject.getAttribute('data-imagealt');
    const description = futuristicObject.getAttribute('data-description');

    changeModal(title, imageSrc, imageAlt, description);
  });
});

document.getElementById('modal-close').addEventListener('click', () => {
  Promise.resolve().then(() => {
    changeModal('', '', '', '');
  });
});
