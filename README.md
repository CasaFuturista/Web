# Web

## Introducción

Este es un proyecto para la asignatura de Diseño de Interfaces Web en el ciclo de Desarrollo de Aplicaciones Web del IES El Rincón (Las Palmas, España) consistente en la realización de un sitio web para presentar una hipotética, pero posible, casa del futuro.

El proyecto desarrollará la descripción visual de una casa futurista a modo de sitio web. Dicha casa presentará un diseño moderno con ampliaciones futuristas e inteligentes.

En el centro de la aplicación aparecerá una imagen 2D isométrica estática de la casa.

El sitio web estará diseñado no a modo de guía, sino de descripción de cada una de las partes y los elementos de la casa, a los cuales se podrá acceder haciendo click en la parte de la casa que sea de interés o a través de un menú de navegación.

## Estructura

### Inicio

La página de inicio, o del índice principal, será el modelo de la casa final en el que cada parte, al pasar el ratón por encima, quedará resaltada con una transición e indicará a qué parte de la casa llevará.

### Secciones

Cada sección de la casa mostrará una imagen de la habitación de trasfondo, con elementos al que serán resaltados al pasar el ratón por encima.

Al hacer click izquierdo en ellos o pulsarlos, se mostrará un desplegable y se trazará una flecha hacia el mismo en una zona cercana, en el que estarán escritas las características de dicho elemento, visualizándose una ficha técnica con la imagen, nombre y descripción del elemento.

Por ejemplo, al hacer click en una «nevera holográfica», se visualizará una ficha con la descripción y nombre de dicho elemento.

### Navegación

En la parte superior central de la página, habrá un menu hamburguesa. Al hacerle click, desplegará hacia abajo un menú principal, con botones para cada sección (y desplegables extra en caso de ser necesario), animado como una cortina. Al hacer click en el menú, se cerrará de nuevo, cerrándose como una cortina.

Sobre la imagen de cada sección, aparecerán 2 botones a los laterales y uno debajo. Estos 2 primeros botones permitirán desplazarse entre las secciones adyacentes, y el de debajo permitirá ir a la sección contenedora. Todos dispondrán de un nombre indicando el destino y, el de debajo tendrá, adicionalmente, el nombre de la sección.

### Versión móvil

La versión móvil será similar en diseño y apariencia a la versión de escritorio.

En este caso, habrá un menú hamburguesa con enlaces a las distintas zonas de la casa y contará con una animación al abrirse y cerrarse (persiana).

El menú hamburguesa tendrá submenús según la zona (piso 1, piso 2, piso 3, exterior), estando en cada submenú un enlace a la zona. Por ejemplo, tendríamos el **submenú** «piso 1» y debajo del mismo se verán los elementos que se encuentren en el primer piso

Al pulsar en un elemento de la imagen de la casa, aparecerá un desplegable con el nombre del elemento, teniendo un botón para acceder a su sección. Al hacer click o pulsar fuera, desaparecerá.

Asimismo, esto ocurrirá con los elementos de una sección, pero mostrando título y descripción en vez de un botón a una sección.

## Tecnologías

### Codeberg
Es la instancia de Gitea (https://gitea.io/) donde hemos hospedado nuestro repositorio del proyecto. Se puede acceder al mismo desde https://codeberg.org/.

### Inskcape
Es el software que hemos utilizado para elaborar las imágenes isométricas de la casa. Se puede descargar desde https://inskcape.org/.

### SweetHome3D
Es el software que hemos utilizado para elaborar las imágenes desde arriba de las habitaciones. Se puede descargar desde https://www.sweethome3d.com/.

### LibreSprite
Es el software que hemos utilizado para elaborar las animaciones de los objetos futuristas. Se puede descargar desde https://libresprite.github.io/.

### Taiga
Es el software que hemos utilizado para organizarnos con la metodología SCRUM. La instancia que hemos utilizado se puede acceder desde https://tree.taiga.io/.

### Pencil
Es el software que hemos utilizado para elaborar el Mockup del sitio web del proyecto. Se puede descargar desde https://pencil.evolus.vn/.